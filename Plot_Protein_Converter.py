#!/usr/bin/env python

#Plot Protein Converter

#This program processes GnomAD csv files and converts them into
#"mutation files" for the R-based application Plot Protein
#by Turner, 2013. It contains three parts:

#1. Setup 
#2. Filtering GnomAD Data
#3. Generating Plot Protein File


#---------------------------------1. Setup---------------------------------


#Imports required modules
import pandas as pd
import numpy as np
import argparse

#Defines command line arguments

parser = argparse.ArgumentParser()
parser.add_argument('-g',
                    '--gnomad_csv',
                    dest='gnomad_csv',
                    required=True,
                    help='Path to GnomAD csv file',
                    default=None)
parser.add_argument('-n',
                    '--gene_name',
                    dest='gene_name',
                    required=True,
                    help='Name of the gene',
                    default=None)
parser.add_argument('-p',
                    '--prot_ID',
                    dest='prot_ID',
                    required=True,
                    help='Protein ID (e.g.: NCBI or UniProt)')
parser.add_argument('-s',
                    '--save',
                    dest='save',
                    required=True,
                    help='Path to save output file',
                    default=None)
args = parser.parse_args()


#-------------------------2. Filtering GnomAD Data-------------------------


def filter_data(gnomad_csv):

    #Converts gnomad csv file to dataframe
    gnomad_frame = pd.read_csv(gnomad_csv, sep=None, engine='python')

    #Selects relevant columns and renames them with underscores
    gnomad_frame = gnomad_frame[['Filters - exomes','Protein Consequence',\
                                 'VEP Annotation','ClinVar Clinical Significance']]
    cols = ['Filters_exomes','Protein_Consequence','VEP_Annotation',\
            'ClinVar_Clinical_Significance']
    gnomad_frame.columns = cols

    #Filters for missense variants from exome sequencing data
    gnomad_frame =gnomad_frame[gnomad_frame['VEP_Annotation']=='missense_variant']
    gnomad_frame =gnomad_frame[gnomad_frame['Filters_exomes']=='PASS']
    gnomad_frame = gnomad_frame.drop(['VEP_Annotation','Filters_exomes'],axis=1)

    #Filters out potentially pathogenic variants
    ClinVar = ['Uncertain significance','Likely pathogenic','Pathogenic',\
               'Conflicting interpretations of pathogenicity']
    gnomad_frame = gnomad_frame[~gnomad_frame.ClinVar_Clinical_Significance.\
                                isin(ClinVar)]
    gnomad_frame = gnomad_frame.drop(['ClinVar_Clinical_Significance'],axis=1)

    #Function that slices the 'Protein_Consequence' column
    #e.g.: "p.Ala589Thr" to "Ala","589", and "Thr" ("p." is discarded)
    def slicer(df):
        df['slice1'] = df['Protein_Consequence'].str.slice(stop=2)
        df['slice2'] = df['Protein_Consequence'].str.slice(start=2)
        df['Original_Res'] = df['slice2'].str.slice(stop=3)
        df['slice3'] = df['slice2'].str.slice(start=3)
        df['Res_Num'] = df['slice3'].str.slice(stop=-3)
        df['New_Res'] = df['slice3'].str.slice(start=-3)

    slicer(gnomad_frame)

    #Reorganizes the data frame into four columns in appropriate order
    gnomad_frame = gnomad_frame.drop(['Protein_Consequence','slice1','slice2',\
                                      'slice3'],axis=1)
    return gnomad_frame

data = filter_data(args.gnomad_csv)


#---------------------3. Generating Plot Protein File----------------------


#Converts residue names from 3-letter to 1-letter format
def converter(res,new_list):
    if res == 'Ala':
        new_list.append('A')
    elif res == 'Arg':
        new_list.append('R')
    elif res == 'Asn':
        new_list.append('N')
    elif res == 'Asp':
        new_list.append('D')
    elif res == 'Cys':
        new_list.append('C')
    elif res == 'Gln':
        new_list.append('Q')
    elif res == 'Glu':
        new_list.append('E')
    elif res == 'Gly':
        new_list.append('G')
    elif res == 'His':
        new_list.append('H')
    elif res == 'Ile':
        new_list.append('I')
    elif res == 'Leu':
        new_list.append('L')
    elif res == 'Lys':
        new_list.append('K')
    elif res == 'Met':
        new_list.append('M')
    elif res == 'Phe':
        new_list.append('F')
    elif res == 'Pro':
        new_list.append('P')
    elif res == 'Ser':
        new_list.append('S')
    elif res == 'Thr':
        new_list.append('T')
    elif res == 'Trp':
        new_list.append('W')
    elif res == 'Tyr':
        new_list.append('Y')
    elif res == 'Val':
        new_list.append('V')
    else:
        print('Error')
        
Original_Res = list(data.Original_Res)
New_Res = list(data.New_Res)

o_res = []
n_res = []

for x in Original_Res:
    converter(x,o_res)
    
for x in New_Res:
    converter(x,n_res)

data = data.drop(['Original_Res','New_Res'],axis=1)
data.insert(1,'Original_Res',o_res)
data.insert(2,'New_Res',n_res)
    
#Adds user-defined gene name and protein ID 
gene_name = args.gene_name
prot_ID = args.prot_ID
data.insert(0, 'Gene_Name', gene_name)
data.insert(1, 'Prot_ID', prot_ID)

#Gets user path to save result file
results_path = args.save
np.savetxt(results_path,data.values,fmt='%s',delimiter='\t')

#Prints echo message
print('Your job has been completed! Please check the destination file.')

#End.
